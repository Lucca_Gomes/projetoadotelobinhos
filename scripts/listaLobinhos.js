let campoLobos = document.querySelector(".areaLobos");


function loboEsquerda(lobo){
    let loboName = document.createElement("h3");
    loboName.className = "loboName";
    loboName.innerText = lobo.nome;
    let loboAge = document.createElement("p");
    loboAge.className = "loboAge";
    loboAge.innerHTML = "Idade: " + lobo.idade + " Anos";
    let loboInfoTexts = document.createElement("div");
    loboInfoTexts.className = "loboInfoTexts";
    loboInfoTexts.appendChild(loboName);
    loboInfoTexts.appendChild(loboAge);
    let loboInfoButton = document.createElement("button");
    loboInfoButton.className = "loboInfoButton";
    loboInfoButton.innerHTML = "Adotar";
    let loboInfoHeader = document.createElement("div");
    loboInfoHeader.className = "loboInfoHeader";
    loboInfoHeader.appendChild(loboInfoTexts);
    loboInfoHeader.appendChild(loboInfoButton);
    let loboInfoDescription = document.createElement("p");
    loboInfoDescription.className = "loboInfoDescription";
    loboInfoDescription.innerText = lobo.descricao;
    let loboInfo =document.createElement("div");
    loboInfo.className = "loboInfo";
    loboInfo.appendChild(loboInfoHeader);
    loboInfo.appendChild(loboInfoDescription);
    let loboImage =document.createElement("img");
    loboImage.className = "loboImage";
    loboImage.src = lobo.imagem;
    let loboContainer = document.createElement("div");
    loboContainer.className = "loboContainer";
    loboContainer.appendChild(loboImage);
    loboContainer.appendChild(loboInfo);

    campoLobos.appendChild(loboContainer);
    loboInfoButton.addEventListener("click", () => {
        window.location.href = './showLobinho.html';
    });
}

function loboDireita(lobo){
    let loboName = document.createElement("h3");
    loboName.className = "loboName";
    loboName.innerText = lobo.nome;
    let loboAge = document.createElement("p");
    loboAge.className = "loboAge";
    loboAge.innerHTML = "Idade: " + lobo.idade + " Anos";
    let loboInfoTexts = document.createElement("div");
    loboInfoTexts.className = "loboInfoTexts";
    loboInfoTexts.appendChild(loboName);
    loboInfoTexts.appendChild(loboAge);
    let loboInfoButton = document.createElement("button");
    loboInfoButton.className = "loboInfoButton";
    loboInfoButton.innerHTML = "Adotar";
    let loboInfoHeader = document.createElement("div");
    loboInfoHeader.className = "loboInfoHeaderD";
    loboInfoHeader.appendChild(loboInfoTexts);
    loboInfoHeader.appendChild(loboInfoButton);
    let loboInfoDescription = document.createElement("p");
    loboInfoDescription.className = "loboInfoDescription";
    loboInfoDescription.innerText = lobo.descricao;
    let loboInfo =document.createElement("div");
    loboInfo.className = "loboInfoD";
    loboInfo.appendChild(loboInfoHeader);
    loboInfo.appendChild(loboInfoDescription);
    let loboImage =document.createElement("img");
    loboImage.className = "loboImage";
    loboImage.src = lobo.imagem;
    let loboContainer = document.createElement("div");
    loboContainer.className = "loboContainerD";
    loboContainer.appendChild(loboImage);
    loboContainer.appendChild(loboInfo);

    campoLobos.appendChild(loboContainer);
    loboInfoButton.addEventListener("click", () => {
        window.location.href = './showLobinho.html';
    });
}

function loboAdotadoEsquerda(lobo){
    let loboName = document.createElement("h3");
    loboName.className = "loboName";
    loboName.innerText = lobo.nome;
    let loboAge = document.createElement("p");
    loboAge.className = "loboAge";
    loboAge.innerHTML = "Idade: " + lobo.idade + " Anos";
    let loboInfoTexts = document.createElement("div");
    loboInfoTexts.className = "loboInfoTexts";
    loboInfoTexts.appendChild(loboName);
    loboInfoTexts.appendChild(loboAge);
    let loboInfoButton = document.createElement("button");
    loboInfoButton.className = "loboAdotadoInfoButton";
    loboInfoButton.innerHTML = "Adotado";
    let loboInfoHeader = document.createElement("div");
    loboInfoHeader.className = "loboInfoHeader";
    loboInfoHeader.appendChild(loboInfoTexts);
    loboInfoHeader.appendChild(loboInfoButton);
    let loboInfoDescription = document.createElement("p");
    loboInfoDescription.className = "loboInfoDescription";
    loboInfoDescription.innerText = lobo.descricao;
    let loboInfoAdotador = document.createElement("p");
    loboInfoAdotador.className = "loboInfoAdotador"
    loboInfoAdotador.innerText = "Adotado por: " + lobo.nomeDono;
    let loboInfo =document.createElement("div");
    loboInfo.className = "loboInfo";
    loboInfo.appendChild(loboInfoHeader);
    loboInfo.appendChild(loboInfoDescription);
    let loboImage =document.createElement("img");
    loboImage.className = "loboImage";
    loboImage.src = lobo.imagem;
    let loboContainer = document.createElement("div");
    loboContainer.className = "loboContainer";
    loboContainer.appendChild(loboImage);
    loboContainer.appendChild(loboInfo);

    campoLobos.appendChild(loboContainer);
    loboInfoButton.addEventListener("click", () => {
        window.location.href = './showLobinho.html';
    });
}

function loboAdotadoDireita(lobo){
    let loboName = document.createElement("h3");
    loboName.className = "loboName";
    loboName.innerText = lobo.nome;
    let loboAge = document.createElement("p");
    loboAge.className = "loboAge";
    loboAge.innerHTML = "Idade: " + lobo.idade + " Anos";
    let loboInfoTexts = document.createElement("div");
    loboInfoTexts.className = "loboInfoTexts";
    loboInfoTexts.appendChild(loboName);
    loboInfoTexts.appendChild(loboAge);
    let loboInfoButton = document.createElement("button");
    loboInfoButton.className = "loboAdotadoInfoButton";
    loboInfoButton.innerHTML = "Adotado";
    let loboInfoHeader = document.createElement("div");
    loboInfoHeader.className = "loboInfoHeaderD";
    loboInfoHeader.appendChild(loboInfoTexts);
    loboInfoHeader.appendChild(loboInfoButton);
    let loboInfoDescription = document.createElement("p");
    loboInfoDescription.className = "loboInfoDescription";
    loboInfoDescription.innerText = lobo.descricao;
    let loboInfoAdotador = document.createElement("p");
    loboInfoAdotador.className = "loboInfoAdotador"
    loboInfoAdotador.innerText = "Adotado por: " + lobo.nomeDono;
    let loboInfo =document.createElement("div");
    loboInfo.className = "loboInfoD";
    loboInfo.appendChild(loboInfoHeader);
    loboInfo.appendChild(loboInfoDescription);
    loboInfo.appendChild(loboInfoAdotador);
    let loboImage =document.createElement("img");
    loboImage.className = "loboImage";
    loboImage.src = lobo.imagem;
    let loboContainer = document.createElement("div");
    loboContainer.className = "loboContainerD";
    loboContainer.appendChild(loboImage);
    loboContainer.appendChild(loboInfo);

    campoLobos.appendChild(loboContainer);
    loboInfoButton.addEventListener("click", () => {
        window.location.href = './showLobinho.html';
    });
}



function showLobinhos(lobinhosDados){
    console.log(lobinhosDados)
    for(const lobo of lobinhosDados){
        if(lobo.id % 2 == 0){
            if(lobo.adotado == false){
                loboEsquerda(lobo);
            }else{
                loboAdotadoEsquerda(lobo);
            }
        }else{
            if(lobo.adotado == false){
                loboDireita(lobo);
            }else{
                loboAdotadoDireita(lobo);
            }    
        }
    }
}



fetch('../scripts/lobinhos.json')
  .then(response => response.json())
  .then(lobinhosDados => showLobinhos(lobinhosDados))
  .catch(error => console.error('Erro ao carregar o arquivo JSON:', error));