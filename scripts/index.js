let imagem1 = document.querySelector("#img1");
let imagem2 = document.querySelector("#img2");
let nome1 = document.querySelector("#nome1");
let idade1 = document.querySelector("#idade1");
let text1 = document.querySelector("#text1");
let nome2 = document.querySelector("#nome2");
let idade2 = document.querySelector("#idade2");
let text2 = document.querySelector("#text2");


function show2Lobinhos(lobinhosDados){
    let num1 = Math.floor(Math.random() * (1001 - 1) + 1);
    console.log(num1)
    let num2 = Math.floor(Math.random() * (1001 - 1) + 1);

    let lobo1 = lobinhosDados.find(lobo => lobo.id === num1);
    console.log(lobo1)
    let lobo2 = lobinhosDados.find(lobo => lobo.id === num2);

    imagem1.src = lobo1.imagem;
    imagem2.src = lobo2.imagem;
    nome1.innerHTML = lobo1.nome;
    nome2.innerHTML = lobo2.nome;
    idade1.innerHTML = "Idade: " + lobo1.idade + " Anos";
    idade2.innerHTML = "Idade: " + lobo2.idade + " Anos";
    text1.innerHTML = lobo1.descricao;
    text2.innerHTML = lobo2.descricao;
}

fetch('./scripts/lobinhos.json')
  .then(response => response.json())
  .then(lobinhosDados => show2Lobinhos(lobinhosDados))
  .catch(error => console.error('Erro ao carregar o arquivo JSON:', error));