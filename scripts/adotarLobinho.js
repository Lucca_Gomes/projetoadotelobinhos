let imagemLobo = document.querySelector(".fotoLobo");
let tituloLobo = document.querySelector(".infoLoboTitle");
let textoLobo = document.querySelector(".infoLoboText");
let nomeDono = document.querySelector(".nomeDono");
let idadeDono = document.querySelector(".idadeDono");
let emailDono = document.querySelector(".emailDono");
let botaoAdotar = document.querySelector(".adotarBotao");

function adotarLobinho(lobinhosDados){
    let loboDado = lobinhosDados.find(lobo => lobo.id === 1);
    imagemLobo.src = loboDado.imagem;
    tituloLobo.innerHTML = "Adotar a(o) " + loboDado.nome;
    textoLobo.innerHTML = "ID: " + loboDado.id;

    botaoAdotar.addEventListener("click", () => {
        nomeInput = nomeDono.value;
        idadeInput = idadeDono.value;
        emailInput = emailDono.value;
        if(nomeInput == "" || idadeInput == "" || emailInput == ""){
            alert("Preencha todos os campos!");
        }else{
            loboDado.adotado = true;
        loboDado.nomeDono = nomeInput;
        loboDado.idadeDono = idadeInput;
        loboDado.emailDono = emailInput;
        console.log(lobinhosDados);
        nomeDono.value = "";
        idadeDono.value = "";
        emailDono.value = "";
        alert("Lobinho Adotado!");
        }
    })
}


fetch('../scripts/lobinhos.json')
  .then(response => response.json())
  .then(lobinhosDados => adotarLobinho(lobinhosDados))
  .catch(error => console.error('Erro ao carregar o arquivo JSON:', error));