let nome = document.querySelector("#nameLobo");
let idade = document.querySelector("#ageLobo");
let foto = document.querySelector("#imgLobo");
let descricao = document.querySelector("#descriptionLobo");
let botaoAdicionar = document.querySelector(".formularioBotao");



function adicionarLobinho(lobinhosDados) {
    function encontrarMaiorId() {
        let maiorId = 0;
        for (const lobo of lobinhosDados) {
          if (lobo.id > maiorId) {
            maiorId = lobo.id;
          }
        }
        return maiorId;
      }
    
    botaoAdicionar.addEventListener("click", () => {
        nomeInput = nome.value;
        idadeInput = idade.value;
        fotoInput = foto.value;
        descricaoInput = descricao.value;
        if(nomeInput == "" || idadeInput == "" || fotoInput == "" || descricaoInput == ""){
            alert("Insira todos os dados!");
        }else{
            const novoId = encontrarMaiorId() + 1;
            const novoLobinho = {
                id: novoId,
                nome: nomeInput,
                idade: idadeInput,
                descricao: descricaoInput,
                imagem: fotoInput,
                adotado: false,
                nomeDono: null,
                idadeDono: null,
                emailDono: null,
            };
        lobinhosDados.push(novoLobinho);
        console.log(lobinhosDados);
        nome.value = "";
        idade.value = "";
        foto.value = "";
        descricao.value = "";
        }   
    })
}


fetch('../scripts/lobinhos.json')
  .then(response => response.json())
  .then(lobinhosDados => adicionarLobinho(lobinhosDados))
  .catch(error => console.error('Erro ao carregar o arquivo JSON:', error));